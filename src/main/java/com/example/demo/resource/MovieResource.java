/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.resource;

import com.example.demo.entities.Movies;
import com.example.demo.service.DetailsService;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import javax.validation.Valid;
import javax.xml.ws.ResponseWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Godah
 */
@CrossOrigin
@RestController
public class MovieResource {
    
    @Autowired
    private DetailsService detailsService = null;
    
    
    @GetMapping("/all")
    public ResponseEntity<List<Movies>> findAll() {
        return ResponseEntity.ok(detailsService.findAll());
    }
    @PostMapping("/create")
    public ResponseEntity createMovie(@Valid @RequestBody Movies movies) {
        
        
        Random rand = new Random(); 
  
        // Generate random integers in range 0 to 999 
        long rand_int1 = rand.nextInt(1000);
        
        movies.setMovieId(rand_int1);
        
        //System.out.print(movies.getMovieName());
        
        return ResponseEntity.ok(detailsService.save(movies));
    }
     @GetMapping("/{id}")
    public ResponseEntity<Movies> findById(@PathVariable Long id) {
        
        Optional<Movies> movie = detailsService.findById(id);
        if (!movie.isPresent()) {
            
            
            //log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(movie.get());
        //return ResponseEntity.ok(detailsService.findByMovieId(id));
    }
    @PutMapping("/{id}")
    public ResponseEntity<Movies> update(@PathVariable Long id, @Valid @RequestBody Movies movie) {
        if (!detailsService.findById(id).isPresent()) {
            //log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(detailsService.save(movie));
    }
    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        if (!detailsService.findById(id).isPresent()) {
            //log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        detailsService.deleteById(id);

        return ResponseEntity.ok().build();
    }
    
}
