/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.service;

import com.example.demo.entities.Movies;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;

/**
 *
 * @author VIN
 */
@Service
public interface DetailsService {
    
    Movies save(Movies movies);
    
    public List<Movies> findAll();       
            
    public Optional<Movies> findById(Long id);
    
    public List<Movies> findByRatings(int rating);
    
    public void deleteById(Long id);
}
