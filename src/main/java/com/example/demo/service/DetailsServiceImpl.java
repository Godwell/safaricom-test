/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.service;

import com.example.demo.entities.Movies;
import com.example.demo.repository.MovieRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Godah
 */
@Service
@Transactional
public class DetailsServiceImpl implements DetailsService{

    @Autowired
    private final MovieRepository movieRepository;
    
    public DetailsServiceImpl(MovieRepository movieRepository){
        this.movieRepository = movieRepository;
    }
    
    @Override
    public Movies save(Movies movies) {
        return movieRepository.save(movies);
    }

    @Override
    public List<Movies> findAll() {
        return movieRepository.findAll();
    }

    @Override
    public Optional<Movies> findById(Long id) {
        return movieRepository.findById(id);
    }

    @Override
    public void deleteById(Long id) {
        movieRepository.deleteById(id);
    }   

    @Override
    public List<Movies> findByRatings(int rating) {
        return movieRepository.findByRating(rating);
    }

    
}
